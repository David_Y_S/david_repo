/** 
 * main.js 
 * This file contains code that runs on load for index.html
 */
// TODO: Write the function displayLockers
/*
define function displayLockers with one parameter data
	define variabel href and assign in document reference by ID to the lockerDisplay DOM
	define variable output and assign it an empty string
	
	use a for loop iterate through the data for every element i in data
		define variable lockers_array and assign it data[i]
		define variable howMany and assign it the length of lockers_array
		use a for loop, iterate from i=0 up to howMany
			define variable single and assign it lockers_array[i] for every iteration
			define variable state assign it locker attribute locked
			define variable id assign it locker attribute id
			define variable label assign it locker attribute label
			define variable colour assign it locker attribute colour
			define variable lockerLock assign it an empty string
			use an if state find whether variable state containus true or false
			case true
				assign lockerLock string lock_closed
			case false
				assign lockerLock string lock_open
			append output with the proper HTML string
		with href manipulate DOM with inner.HTML and assign it variable output	
*/
function displayLockers(data){
	let href = document.getElementById("lockerDisplay");
	var output = "";
	for(let i in data){
		let lockers_array = data[i];
		let howMany = lockers_array.length;
		for(let i = 0; i<howMany; i++){
			let single = lockers_array[i];
			let state = single.locked;
			let id = single.id;
			let label = single.label;
			let colour = single.color;
			let lockerLock = "";
			if(state === true){
				lockerLock = 'lock_closed';
			} else { 
				lockerLock = 'lock_open';
			}
		
			output +=   "<div class=\"mdl-cell mdl-cell--4-col\">" +
                        "<div class=\"mdl-card mdl-shadow--2dp locker\" style=\"background-color:#"+ colour +"\">" +
                            "<div class=\"mdl-card__title mdl-card--expand\">" +
                                "<h2>" + id + "</h2>" +
                                "<h4>&nbsp;" + label + "</h4>" +
                            "</div>" +
                            "<div class=\"mdl-card__actions mdl-card--border\">" +
                                "<a class=\"mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect\"" +
                                "onclick=\"view(" + i + ")\">Open Locker</a>" +
                                "<div class=\"mdl-layout-spacer\"></div>" +
                                "<i class=\"material-icons\">" + lockerLock + "</i>" +
                            "</div>" +
                        "</div>" +
                    "</div>"
		}
    }
  	href.innerHTML = output; 
}
        
// TODO: Write the function addNewLocker
/*
define function addNewLocker with no parameter
	define variable list assign it function call getDataLocalStorage
	use a for loop iterate all element i in variable list
		define variable lockersArray assign it list[i] for every iteration
		define variable last assign it value of length of variable lockersArray
		use an if statement if case true last ===0
		case true
			use global variable lockers and method addLocker with id parameter 1
			call function updateLocalStorage input global variable lockers
			call function displayLockers passing global variable lockers
		case false
			use global variable lockers and method addLocker with id parameter last+!
			call function updateLocalStorage input global variable lockers
			call function displayLockers passing global variable lockers
*/
function addNewLocker(){
		let list = getDataLocalStorage();
			for(let i in list){
				let lockersArray = list[i];
				let last = lockersArray.length;
				if(last === 0){
					lockers.addLocker(1);
					updateLocalStorage(lockers);
					displayLockers(lockers);
				} else {
	 				lockers.addLocker(last+1);
					updateLocalStorage(lockers);
					displayLockers(lockers);
				}
		}
}
/*
define function view with parameter index
	define variable updated assign it parameter index
	set variable updated as value to the local storage with key LOCKER_INDEX_KEY
	change window to view.html
*/

// TODO: Write the function view
function view(index){
	let updated = index;
	localStorage.setItem(LOCKER_INDEX_KEY , updated);
	window.location = "view.html";
}

// TODO: Write the code that will run on load here
/*
define variable let assign it function call getDataLocalStorage
global variable lockers method fromData pass input value variable data
function call displayLockers
*/

let data = getDataLocalStorage();
lockers.fromData(data)
displayLockers(lockers)

