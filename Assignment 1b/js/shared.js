
// Constants used as KEYS for LocalStorage
const LOCKER_INDEX_KEY = "selectedLockerIndex";
const LOCKER_DATA_KEY = "lockerLocalData";
// TODO: Write code to implement the Locker class 
class Locker
{
	constructor(id)
	{
		// private attributes
		this._id = "A" + id;
		this._label = "";
		this._locked = false;
		this._pin = "";
		this._color = "3399ff";
		this._contents = "";
	
	}
		// accessors
		get id() {return this._id; }
		get label() {return this._label; }
		get locked() {return this._locked; }
		get pin() {return this._pin; }
		get color() {return this._color; }
		get contents() {return this._contents; }
	
	//mutators
	set label(text)
	{
			this._label = text;
	}
	set locked(state)
	{
			this._locked = state;
	}
	set pin(pin)
	{
			this._pin = pin;
	}
	set color(color)
	{
			this._color= color;
	}
	set contents(text)
	{
			this._contents= text;
	}
    fromData(dataObject){
        this._id = dataObject._id;
        this._label = dataObject._label;
        this._locked = dataObject._locked;
        this._pin = dataObject._pin;
        this._color = dataObject._color;
        this._contents = dataObject._contents;
       
    }  
}
 
// TODO: Write code to implement the LockerList class
class LockerList
{
	constructor()
	{
		// private attributes
		this._lockers = [];
	
	}
		// accessors
		get lockers() {
          return this._lockers; }
		get count() { 
          let array = this._lockers;
          return array.length;}
	
	//methods
	addLocker (id)
	{
		this._lockers.push(new Locker(id));
	}
	getLocker (index)
	{	
		return this._lockers[index];
	}
	removeLocker (id)
	{
		this._lockers.splice(id,1);
	}
	fromData(dataObj)
    {
		let a = [];
        	for(let elements in dataObj){
				let theList = dataObj[elements];
				let howMany= theList.length;
				for(let i =0; i<howMany; i++){
					let single = theList[i];
        			let single_constructed_locker = new Locker();
        			single_constructed_locker.fromData(single);
					a.push(single_constructed_locker);
				}
     		}
		this._lockers = a;
    }			
}



// TODO: Write the function checkIfDataExistsLocalStorage
/* 
define function checkIfDataExistLocalStorage with no parameter
	use an if statement find if local storage is available
	case true use an if else if 
		if else find either null, undefined, or empty string return false
		else return true
	case false return false
*/
function checkIfDataExistsLocalStorage(){
	if (typeof(Storage) !== "undefined"){
  		let parsed = JSON.parse(localStorage.getItem(LOCKER_DATA_KEY));
  		if(parsed === null){
     		return false;
  		} else if(parsed === ""){
     		return false;
  		} else if (typeof(parsed) === "undefined" ){
     		return false;
  		} else {        
     		return true;
  		} 
	}else{
		return false;
	}
}



// TODO: Write the function updateLocalStorage
/*
define function updateLocalStorage with parameter data
	define variable updated and assign it JSON stringify parameter
	set to local storage variable updated with key LOCKER_DATA_KEY
*/
function updateLocalStorage(data){
	let updated = JSON.stringify(data);
	localStorage.setItem(LOCKER_DATA_KEY, updated);
}


// TODO: Write the function getDataLocalStorage
/*
define function getDataLocalStorage with no parameter
	define variable object and assign in the JSON parse value from local storage with key LOCKER_DATA_KEY
	return variable object
*/
function getDataLocalStorage(){
	object = JSON.parse(localStorage.getItem(LOCKER_DATA_KEY));
	return object
}



// Global LockerList instance variable
let lockers = new LockerList();



// TODO: Write the code that will run on load here
/*
use an if statement for checkIfDataExistsLocalStorage()
	case true 
		define variable object and assign in getDataLocalStorage
		use the global variable lockers and using the method .fromData with input variable object
	case false
		use method addLocker to the global variable lockers and addLocker with input id 1
		call function updateLocalStorage
*/
if(checkIfDataExistsLocalStorage() === true){
	let object = getDataLocalStorage();
    lockers.fromData(object);
} else {
	lockers.addLocker(1);
	updateLocalStorage(lockers);
}



