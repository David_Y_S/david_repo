/** 
 * view.js 
 * This file contains code that runs on load for view.html
 */

"use strict";
// TODO: Write the function displayLockerInfo
/*
define function displayLockerInfo with parameter locker
	define variable refId assign it DOM element by id LockerId
	define variable refLabelInp assign it DOM element by id lockerLabel
	define variable refLabelDisp assign it DOM element by id labelDisplay
	define variable refcContents assign it DOM element by id lockerContents
	define variable refColor assign it DOM element by id lockerColor
	from each reference assign the locker.attr respectively according to the variable name
	
*/
function displayLockerInfo(locker){
	let refId = document.getElementById("LockerId");
	let refLabelInp = document.getElementById("lockerLabel");
	let refLabelDisp = document.getElementById("labelDisplay");
	let refContents = document.getElementById("lockerContents");
	let refColor = document.getElementById("lockerColor");
	refId.innerHTML = locker.id;
	refLabelInp.value = locker.label;
	refLabelDisp.innerHTML = locker.label;
	refContents.innerHTML = locker.contents;
	refColor.value = locker.color;
	
}

// TODO: Write the function unlock
/*
define function unlock parameter locker
	define variabel pinEnterd and primpt user for a pin
	use an if statemnet true for pinEntered === locker attribute pin
	case true
		locker locked attribute is now false
		locker pin attribute is now an empty string
		call function displayLockerInfo pass locker variable as input
	case false
		alert pin entered does not match
		window change to index.html
*/
function unlock(locker){
	let pinEntered = prompt("Please enter the pin:");
	if(pinEntered === locker._pin){
		locker.locked = false;
		locker.pin = "";
		displayLockerInfo(locker);
	}else{
		alert("Pin entered is wrong!");
		window.location = "index.html";
	}
}

// TODO: Write the function deleteThisLocker
/*
define function deletThisLocker
	alert that locker is to be deleted
	use if statement confirm with the user if true
	case true
		global variable lockers and method removeLocker with parameter index
		function call updateLocalStorage with input global variable lockers
		alert has been deleted
		window change to index.html
	case false
		window change to index.html
*/
	function deleteThisLocker(){
		alert("do you want to delete this locker?");
		if (confirm("Are you very sure?")){
			lockers.removeLocker(index);
			updateLocalStorage(lockers);
			alert("locker has been deleted");
			window.location = "index.html";
		}else{
 			window.location = "index.html";
		}

	}

// TODO: Write the function lockLocker
/*
define function lockLocker no parameter
	alert locking 
	confirm case true yes
	case true
		prompt user twice to input the same pin, variable pin1 prompt and pin2 prompt
		use while loop to check if pin entered is not null
		use if statement case true for pin1 === pin2
		case true
			set locker pin attr to pin1
			set locker attr locked to true
			set locker contents attr to the dom value at id lockerContents
			set locker label attr to the dom value at id lockerLabel
			set locker color attr to the dom value at id lockerColor
			function call updateLocalstorage input global variable lockers
			alert locker is saved and locked
			window to index.html
		case false
			alert error, data is not saved redirecting directly
			window ti index.html
	case false
		alert data is not saved and redrecting directly
		change window to index.html
*/
	function lockLocker(){
		alert("do you want to lock this locker?");
		if (confirm("Lock this locker?")){
			let pin1 = prompt("Please enter the pin:");
			while (pin1 === "") {
  				pin1 = prompt("Please re-enter a proper pin:");
			}
			let pin2 = prompt("Please enter the same pin again:");
			if(pin1 === pin2){
				locker.pin = pin1;
				locker.locked = true;
				locker.contents = document.getElementById("lockerContents").value;
				locker.label = document.getElementById("lockerLabel").value;
				locker.color = document.getElementById("lockerColor").value;
				updateLocalStorage(lockers);
				alert("The locker has been locked. Redirecting to the main page");
				window.location = "index.html";
			} else {
				alert("Pin does not match, locker locking has failed and data are not saved. Redirecting to the main page");
				window.location = "index.html";
			}
		}else{
			alert("Data are not saved. Redirecting to the main page");
 			window.location = "index.html";
		}
	}

// TODO: Write the function closeLocker
/*
define function closeLocker no parameter
	alert close without locking
	if else confirmation 
	case true
		set locker attr locked to false
		set locker contents attr to the dom value at id lockerContents
		set locker label attr to the dom value at id lockerLabel
		set locker color attr to the dom value at id lockerColor
		function call updateLocalstorage input global variable lockers
		alert saving complete but not locked
		window to index.html
	case false
		alert data are not saved and to main page
		window to index.html
	
*/
	function closeLocker(){
		alert("do you want to close the locker without locking?");
		if (confirm("Are you sure?")){
				locker.locked = false;
				locker.contents = document.getElementById("lockerContents").value;
				locker.label = document.getElementById("lockerLabel").value;
				locker.color =document.getElementById("lockerColor").value;
				updateLocalStorage(lockers);
				alert("Saving complete, The locker has been closed but not locked. Redirecting to the main page");
				window.location = "index.html";
		}else{
			alert("Data are not saved. Redirecting to the main page");
 			window.location = "index.html";
		}
	}

// Retrieve the stored index from local storage
let index = localStorage.getItem(LOCKER_INDEX_KEY);
// using the getLocker method, retrieve the current Locker instance
let locker = lockers.getLocker(index);

// TODO: Write the code that will run on load here
/*
if statement case true for locker attribute locked
case true
	function call unlock input variable locker
case false
	function call displayLockerInfo input variable locker
*/
	if(locker.locked === true){
		unlock(locker);
	} else {
		displayLockerInfo(locker);
	}
