/* define function fahrenheitToCelcius() with one parameter
	define a variable celc and assign it to take the parameter and 
	subtract the parameter by 32 first 
	then multiply it by 5 and divide it by 9
	return statement with variable celc using toFixed to 3 decimal places
*/

function fahrenheitToCelcius(tempt){
	celc=(tempt-32)*5/9;
	return celc.toFixed(3);
}

/* function test
console.log(fahrenheitToCelcius(100))
*/