/* define function movingAverage with parameter dataArray
		creata a variable total and assign the number 0 to it
		create a variable amount and assign the length of the parameter with dataArray.length
		use a for loop to iterate through dataArray and find the sum of dataArray
		 	add dataArray value of the current iteration or index to the variable total
		create a variable average dividing variable total with variable amount
		return average
*/

function movingAverage(dataArray){
    let total=0
    let amount=dataArray.length;
	for(let i=0;i<amount;i++){
        total += dataArray[i];
	}
	let average=total/amount;
	return average
}
let dataArray = [515, 523, 512, 504, 564, 535, 526, 596, 577, 508, 530, 
578];
let dataArray1 = [515, 523, 512, 504, 564, 535, 526, 596, 577, 508, 
530, 578, 594, 538, 521, 599];
let dataArray2 = [515, 523, 512, 504, 564, 535, 526, 596, 577, 508, 
530, 578, 594, 538, 521, 599, 561, 585, 584, 550, 567];

/* function test
console.log(movingAverage(dataArray))
console.log(movingAverage(dataArray1))
console.log(movingAverage(dataArray2))
*/