/*define function calculateRatio with 1 parameter radius
    define variable area and let it be pi multiplied by parameter squared
    define variable circ and let it be 2 multiplied by pi and multiplied by parameter
    define variable ratio and let it be variable area divided by variable circ
    return ration
    */

function calculateRatio (radius){
        let area=Math.PI*radius*radius;
        let circ=2*radius*Math.PI;
        let ratio= area/circ
        return ratio
}

/* test function
let radius = 1;

do{
var ratio = calculateRatio (radius)
console.log("Radius: "+radius+", ratio: "+ratio)
radius++
}while(ratio<=30)
*/