/* define function sortCountries with 2 parameters dataArray and order
			use an if else statement check if order is equal to boolean true
			case true assign dataArray sorted ascending with .sort to variable str
			case false assign dataArray sorted descending with .reverse to variable str
	 	return str value
*/
function sortCountries(dataArray,order){
	if(order===true){
		var str=dataArray.sort();
	}else{
		var str=dataArray.reverse()
	}
	return str
}

/* function test
let countryArray =["Sri Lanka","Afghanistan","Malaysia","Botswana","Cameroon","Singapore","Vietnam","India"];
console.log("Ascending : "+sortCountries(countryArray,true));
console.log("Descending : "+sortCountries(countryArray,false));
*/