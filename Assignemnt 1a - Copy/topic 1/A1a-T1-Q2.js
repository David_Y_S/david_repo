/* define function checkMarkValid with a one parameter mark
		assign a null string to variable check
		for loop to iterate from 0 to length of mark parameter
		assign variable each for mark value of each iteration
			use an if statemnet to find if it is a number and is an integer
				case true another if statment
				true being value is 0 to 100 inclusively and assign string "true" to var bool
				if false then assign string "false" to var bool
			case if false then assign string "false" to var bool
		augment variable a with var each and var bool
	return the value of check
*/

function checkMarkValid (mark) {
  	let check="";
	for(let i=0;i<mark.length;i++){
		var each=mark[i];
		if((typeof(each)==="number")&&(Number.isInteger(each))){
           if((each>=0)&&(each<=100)){
			 	var bool="true";
		   }else{
			 	var bool="false";
		   }
		}else{
			var bool="false";
		}
        check += each+": "+bool+"\n";
    }
  return check
}

/*function test
console.log(checkMarkValid([12,45,67,900,"dog",-1,true]))
*/