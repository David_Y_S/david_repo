/*define a function searchInternationStudents with parameter dataArray
		assign 0 to variable numInterStudents 
		use for loop to iterate each property in dataArray
			use an if else statement check nationality is equal to Asutralia
			case true contine
			case false add the value 1 to variable numInterStudents
		return numInterStudents

*/
function searchInternationalStudents(dataArray){
   let numInterStudents=0;
   for(let prop in dataArray){
     if(dataArray[prop].nationality==="Australian"){
       continue
     }else{
       numInterStudents += 1;
     }
   }
   return numInterStudents
 }

/* function test
let students=[
{
id:23978904,
unit:["ENG1003"],
nationality:"Australian"
},
{
id:28976589,
unit:["ENG1003"],
nationality:"American"
},
{
id:28951284,
unit:["ENG1003"],
nationality:"Indian"
},
{
id:36789027,
unit:["ENG1003"],
nationality:"Australian"
},
{
id:22345604,
unit:["ENG1003"],
nationality:"Sri Lankan"
}
];
console.log(searchInternationalStudents(students))
*/