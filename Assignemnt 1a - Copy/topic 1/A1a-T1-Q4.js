/* define a function daySuffix with one parameter num
		assign a null string to variable str
			use an if statement case true the input is a number and is an integer
				case true an if statement being true is input is greater equal to 1 or smaller equal to 31
					an if else statemnet to check 1 or 21 or 31, 2 or 22, 3 or 23
						if true to 1 or 21 or 31 augment var str with the input number and string "st"
						else if true to 2 or 22 augment var str with the input number and string "nd"
						else if true to 3 or 23 augment var str with the input number and string "rd"
						else then augment var str with the input number and string "th"
				case false augment var str with the parameter and null
			case false augment var str with the parameter and null
 		return variable str
*/
	

function daySuffix(num){
	let str="";
	if((typeof(num)==="number")&&(Number.isInteger(num))){
		if(num>=1&&num<=31){
			if(num===1||num===21||num===31){
			str=num + " : " + num + "st";
			}else if(num===2||num===22){
			str=num + " : " + num + "nd";
			}else if(num===3||num===23){
			str=num + " : " + num + "rd";
			}else{
			str=num + " : " + num + "th";
			}
		}else{
		str=num + " : " + null;
		}
	}else{
	str=num + " : " + null;
	}
	return str;
}

/* function testing
let output = "";
for (let i = 1; i <= 31; i++)
{
 output += daySuffix(i) + "\n";
}
output += daySuffix("dog") + "\n";
output += daySuffix(-1) + "\n";
output += daySuffix(100) + "\n";
output += daySuffix("d0g") + "\n";
console.log(output)
*/