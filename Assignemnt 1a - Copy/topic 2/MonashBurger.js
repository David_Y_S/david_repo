/*
define variable strMeat, strSpiceLvl, and strDrink as null string
define variable strCheese, strSauce, and strVeg as null array
define meat, cheese, sauce, veg, spyc, and drink and assign the value 0 to all of them
*/

let strMeat="";
let meat=0;

let strCheese=[];
let cheese=0;

let strSauce=[];
let sauce=0;

let strVeg=[];
let veg=0;

let strSpiceLvl="";
let spcy=0;

let strDrink="";
let drink=0;

let mobile="";

/*
define function getMeat with one parameter ticked
	assign ticked to variable strMeat
	assign value of element ticked value to variable meat
*/

function getMeat(ticked){
		strMeat = ticked;
		meat = Number(document.getElementById(ticked).value);
	console.log(document.getElementById("Chicken").value)
}

/* 
define function getCheese with one parameter ticked
	use an if loop to check whether ticked is present in array strCheese
		case true subtract variable cheese with the value reference to ticked
			a for loop to iterate strCheese
				an if loop to check whether the current iteration is the same as ticked
					case true delete the current element in the array using splice
		case false push ticked to array strCheese and add value with reference to ticked to variable cheese
*/

function getCheese(ticked){
	if(strCheese.includes(ticked)){
		cheese -= (Number(document.getElementById(ticked).value));
		 for(let i=0;i<strCheese.length;i++){
			 if(strCheese[i]===ticked){
				 strCheese.splice(i,1)
			 }
		 }
	}else{
		strCheese.push(ticked);
		cheese += Number(document.getElementById(ticked).value);	
	}		
}

/* 
define function getSauce with one parameter ticked
	use an if loop to check whether ticked is present in array strSauce
		case true subtract variable Sauce with the value reference to ticked
			a for loop to iterate strSauce
				an if loop to check whether the current iteration is the same as ticked
					case true delete the current element in the array using splice
		case false push ticked to array strSauce and add value with reference to ticked to variable Sauce
*/

function getSauce(ticked){
	if(strSauce.includes(ticked)){
		sauce -= (Number(document.getElementById(ticked).value));
		 for(let i=0;i<strSauce.length;i++){
			 if(strSauce[i]===ticked){
				 strSauce.splice(i,1)
			 }
		 }
	}else{
		strSauce.push(ticked);
		sauce += Number(document.getElementById(ticked).value);	
	}		
}

/* 
define function getVeggies with one parameter ticked
	use an if loop to check whether ticked is present in array strVeg
		case true subtract variable Veg with the value reference to ticked
			a for loop to iterate strVeg
				an if loop to check whether the current iteration is the same as ticked
					case true delete the current element in the array using splice
		case false push ticked to array strVeg and add value with reference to ticked to variable Veg
*/

function getVeggies(ticked){
	if(strVeg.includes(ticked)){
		veg -= (Number(document.getElementById(ticked).value));
		 for(let i=0;i<strVeg.length;i++){
			 if(strVeg[i]===ticked){
				 strVeg.splice(i,1)
			 }
		 }
	}else{
		strVeg.push(ticked);
		veg += Number(document.getElementById(ticked).value);	
	}		
}

/*
define function getSpicy with one parameter ticked
	assign value of element ticked value to spcy
	use an if loop to check the value of spcy
		if spcy is 0 assign string "Not spicy" to variable strSpiceLvl
		if spcy is 0.5 assign string "Mildly spicy" to variable strSpiceLvl
		if spcy is 0.75 assign string "Medium spicy" to variable strSpiceLvl
		else assign string "Extra spicy" to variable strSpiceLvl
*/

function getSpicy(ticked){
	spcy = Number(document.getElementById(ticked).value); 
		if(spcy==0){
			strSpiceLvl = "Not spicy"
		}else if(spcy==0.5){
			strSpiceLvl = "Mildly spicy"
		}else if(spcy==0.75){
			strSpiceLvl = "Medium spicy"
		}else{
			strSpiceLvl = "Extra spicy"
		} 
}

/*
define function getDrinks with one parameter ticked
	use an if loop case true variable strDrink is null
		case true, assign ticked value to variable strDrink
		assign number 2 to variable drink
			case false, assign ticked value to variable strDrink
*/

function getDrinks(ticked){
	if(strDrink===""){
		strDrink =  document.getElementById(ticked).value;
		drink = 2;
	}else{
		strDrink =  document.getElementById(ticked).value;
	}
}

/*
define function submitOrder with no parameter
	define variable outputArea with value that refers to resultArea id
	define variable name with value that refers to name id
	define variable orderNum with floored random number that is multiplied by 500
	define variable cost as the sum of variables meat, cheese, sauce, veg, spcy, and drink
	write to innerhtml refering to outputArea the name, the order number, the cost, what are the orders and enjoy
*/

function submitOrder(){
	let outputArea = document.getElementById("resultArea")
	let name=document.getElementById("name").value
	let orderNum=Math.floor(Math.random()*500)
	let cost=meat+cheese+sauce+veg+spcy+drink;
	mobile =document.getElementById("mobile")
	outputArea.innerHTML = name + ", your order number is " + orderNum + " and total cost is $" + cost + "<br>"
	+ "Your " + strMeat + " Burger includes " + strCheese + ", " + strSauce + ", " + strVeg + " and is " + strSpiceLvl 
	+ ". Your drink is "+ strDrink + "	<br>" + "Enjoy!"	
}

