 //task 1
var testObj = {
 number: 1,
 string: "abc",
 array: [5, 4, 3, 2, 1],
 boolean: true
};
function objectToHTML(par1){
	let outputArea = document.getElementById("outputArea1") 
	for(let prop in par1){
	outputArea.innerHTML+=prop+": "+ par1[prop] +"<br>";
	}
}
objectToHTML(testObj)

//task 2
var outputAreaRef = document.getElementById("outputArea2");

var output = "";

function flexible(fOperation, operand1, operand2){
 var result = fOperation(operand1, operand2);
 return result;
}

function flexible1(num1, num2){
	let outputArea = document.getElementById("outputArea2") 
	outputArea.innerHTML+= num1 + num2 +"\n" + "<br/>";
}
function flexible2(num1, num2){
	let outputArea = document.getElementById("outputArea2") 
	outputArea.innerHTML+= num1*num2 +"\n" + "<br/>";
}

outputAreaRef.innerHTML = output;
flexible1(3,5);
flexible2(3,5);

//task 3
/*define function extremeValues(), one parameter
	create variable min and max assigning the two with the 1st element of the parameter
	use for loop, iterate from 2nd element until last element
		use an if statement, true being element i is smaller than current min value
		if true then update variable min to element i
	use 2nd for loop, iterate from 2nd element until last element 
		use an if statement, true being element i is larger than current max value
		if true then update variable max to element i
	return both variable min and max as an array*/	
  
//task 4
function extremeValues(arr){
	let outputArea = document.getElementById("outputArea3") 
	let min=arr[0];
	let max=arr[0];
	for(let i=1;i<arr.length;i++){
		if(min>arr[i]){
			min=arr[i];
		}
	}
	for(let i=1;i<arr.length;i++){
		if(max<arr[i]){
			max=arr[i];
		}
	}
	outputArea.innerHTML=[min,max]+"<br>";
}
extremeValues([4, 3, 6, 12, 1, 3, 8])